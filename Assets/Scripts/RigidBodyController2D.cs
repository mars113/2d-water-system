﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class RigidBodyController2D : MonoBehaviour
{

    public float moveForce = 3f;
    
    public float jumpForce = 3f;

    public bool allowJump = false;

    public bool allowSecondJump = false;

    public Transform groundCheck;

    public LayerMask ground;

    protected Rigidbody2D rb;

    protected SpriteRenderer sr;

    public void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        sr = GetComponent<SpriteRenderer>();
    }

    
    public void Update()
    {
        CalculateMovement();

        CalculateJump();
    }


    public void CalculateMovement()
    {
        float h = Input.GetAxisRaw("Horizontal");

        rb.AddForce(Vector2.right * h * moveForce);

        if(h > 0)
        {
            sr.flipX = false;
        }
        else if(h < 0)
        {
            sr.flipX = true;
        }
    }


    protected void CalculateJump()
    {        
        if (!IsGrounded() && !allowSecondJump)
        {
            return;
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (!allowJump && allowSecondJump)
            {
                allowSecondJump = false;
            }

            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        } 
    }


    protected bool IsGrounded()
    {

        allowJump = Physics2D.OverlapCircle(groundCheck.position, 0.1f, ground);

        if (allowJump)
        {
            allowSecondJump = true;
        }

        return allowJump; 
    }


    #if UNITY_EDITOR
    protected void OnDrawGizmos()
    {
        if (groundCheck != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(groundCheck.position, 0.1f);
        }
    }
    #endif

}
