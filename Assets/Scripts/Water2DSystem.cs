﻿using UnityEngine;
using System.Collections;

public class Water2DSystem : MonoBehaviour
{
    
    #region [變數]

    public Water2DSystemInitParameters initParam;

    public Water2DSystemParameters param;

    public WaterLinePart waterLine;

    public WaterBodyPart[] parts;

    public float[] xPositions;

    public float[] yPositions;

    public float[] velocities;

    public float[] accelerations;    

    int springCount;

    float left;

    public float top;

    BoxCollider2D collider;

    BuoyancyEffector2D effector;

    #endregion


    void Start()
    {
        InitVerticesPos();

        InitWaterLine();

        InitWaterBody();

        InitPhysics();

        AutoWaveFX();
    }


    void FixedUpdate()
    {
        CalculateWater();

        UpdateWaterLine();

        UpdateWaterBoty();

        UpdatePhysics();
    }

    public void CalculateWater()
    {

        for (int i = 0; i < velocities.Length; i++)
        {

            //虎克定律: F = k*x 與牛頓第二運動定律 F = ma,結合可得到 a = k*x/m
            //因為加入了衰減值damping用來表示水體的黏度,故質量m可以忽略不計算, 於是得到新的算式  a = k*x - damping
            accelerations[i] = param.springconstant * (top - yPositions[i]) - velocities[i] * param.damping;

            velocities[i] += accelerations[i];

            yPositions[i] += velocities[i];

        }


        float[] leftDeltas = new float[springCount + 1];

        float[] rightDeltas = new float[springCount + 1];

        //We make 8 small passes for fluidity:
        for (int j = 0; j < 8; j++)
        {
            for (int i = 0; i < yPositions.Length; i++)
            {
                //We check the heights of the nearby nodes, adjust velocities accordingly, record the height differences
                if (i > 0)
                {
                    leftDeltas[i] = param.spread * (yPositions[i] - yPositions[i - 1]);
                    velocities[i - 1] += leftDeltas[i];
                }
                if (i < yPositions.Length - 1)
                {
                    rightDeltas[i] = param.spread * (yPositions[i] - yPositions[i + 1]);
                    velocities[i + 1] += rightDeltas[i];
                }
            }

            //Now we apply a difference in position
            for (int i = 0; i < yPositions.Length; i++)
            {
                if (i > 0)
                    yPositions[i - 1] += leftDeltas[i];

                if (i < yPositions.Length - 1)
                    yPositions[i + 1] += rightDeltas[i];
            }
        }
    }


    public void OnTriggerEnter2D(Collider2D col)
    {
        int id = GetWaveIndexWithPositionX(col);

        if(id > -1)
            velocities[id] = col.attachedRigidbody.velocity.y * 0.1f;       
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        int id = GetWaveIndexWithPositionX(col);

        if (id > -1)
            velocities[id] = col.attachedRigidbody.velocity.y * 0.05f;
    }

    int GetWaveIndexWithPositionX(Collider2D col)
    {
        int id = -1;

        if (col.attachedRigidbody == null)
        {
            return id;
        }

        float offsetX = col.transform.position.x - transform.position.x - xPositions[0] * transform.localScale.x;

        if (offsetX < 0 || offsetX > (xPositions[xPositions.Length - 1] - xPositions[0]) * transform.localScale.x)
        {
            return id;
        }

        id = Mathf.FloorToInt(offsetX / ((xPositions[1] - xPositions[0]) * transform.localScale.x));

        return id;
    }


    void InitPhysics()
    {
        collider = gameObject.AddComponent<BoxCollider2D>();       

        collider.isTrigger = true;

        collider.usedByEffector = true;

        effector = gameObject.AddComponent<BuoyancyEffector2D>();

        effector.linearDrag = 10f;

        effector.angularDrag = 10f;
    }
    

    void AutoWaveFX()
    {
        StartCoroutine(AutoWaveFXTimer());
    }


    IEnumerator AutoWaveFXTimer()
    {
        while(true)
        {

            velocities[0] += 0.03f;

            velocities[velocities.Length - 1] += 0.03f;

            yield return new WaitForSeconds(Random.Range(0.5f, 1f));

        }
    }
    

    /// <summary>
    /// 
    /// </summary>
    public void InitVerticesPos()
    {
        springCount = initParam.waveCount;

        xPositions = new float[springCount + 1];
        yPositions = new float[springCount + 1];

        float deltaX = (initParam.width / springCount) / initParam.pixelPerUnit;
        left = initParam.width * 0.5f / initParam.pixelPerUnit;
        top = initParam.height / initParam.pixelPerUnit;

        for (int i = 0; i < springCount + 1; i++)
        {
            xPositions[i] = deltaX * i - left;
            yPositions[i] = top;
        }

        velocities = new float[springCount + 1];
        accelerations = new float[springCount + 1];
    }


    /// <summary>
    /// 初始化LineRenderer
    /// </summary>
    public void InitWaterLine()
    {
        waterLine.Init(gameObject, initParam.matWaterLine, xPositions, yPositions, initParam.offsetZ);
    }
    
    /// <summary>
    /// 
    /// </summary>
    public void UpdateWaterLine()
    {
        waterLine.UpdateWaterLine(xPositions, yPositions, initParam.offsetZ - 1);
    }

    /// <summary>
    /// 
    /// </summary>
    public void InitWaterBody()
    {
        if (springCount < 1)
            return;

        parts = new WaterBodyPart[springCount];

        for (int i = 0; i < springCount; i++)
        {
            float xUVPoint = 1f / springCount; 

            parts[i] = new WaterBodyPart(transform, initParam.matWaterBody, xPositions[i], xPositions[i + 1], yPositions[i], yPositions[i + 1], initParam.offsetZ, xUVPoint * i, xUVPoint * (i+1));

            parts[i].UpdateVertices(xPositions[i], xPositions[i + 1], yPositions[i], yPositions[i + 1], initParam.offsetZ);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    void UpdateWaterBoty()
    {
        for (int p = 0; p < parts.Length; p++)
        {
            parts[p].UpdateVertices(xPositions[p], xPositions[p+1], yPositions[p], yPositions[p+1], initParam.offsetZ);
        }
    }


    void UpdatePhysics()
    {
        collider.size = new Vector2(xPositions[xPositions.Length-1] - xPositions[0], top);

        collider.offset = Vector2.up * collider.size.y * 0.5f;

        effector.surfaceLevel = top;
    }

}


/// <summary>
/// 
/// </summary>
[System.Serializable]
public class Water2DSystemInitParameters
{

    public Material matWaterLine;

    public Material matWaterBody;

    public float width = 1000;

    public float height = 200;

    public int pixelPerUnit = 100;

    public int waveCount = 30;

    public float offsetZ = -1;

    public bool segmentPreview = true;

}


/// <summary>
/// 
/// </summary>
[System.Serializable]
public class Water2DSystemParameters
{
    
    public float springconstant = 0.02f;       //彈簧勁度係數

    public float damping        = 0.04f;

    public float spread         = 0.05f;

}


/// <summary>
/// 
/// </summary>
[System.Serializable]
public class WaterLinePart
{

    public LineRenderer line;

    public void Init(GameObject go, Material mat, float[] xPos, float[] yPos, float z)
    {

        line = go.GetComponent<LineRenderer>();

        if (line == null)
            line = go.AddComponent<LineRenderer>();

        line.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

        line.receiveShadows = false;

        line.useLightProbes = false;

        line.material = mat;

        line.SetWidth(0.03f, 0.03f);

        line.SetVertexCount(xPos.Length);

        line.useWorldSpace = false;

        UpdateWaterLine(xPos, yPos, z);

    }


    public void UpdateWaterLine(float[] xPos, float[] yPos, float z)
    {
        for(int i = 0; i < xPos.Length; i++)
        {
            line.SetPosition(i, new Vector3(xPos[i], yPos[i], z));
        }
    }

}


/// <summary>
/// 
/// </summary>
[System.Serializable]
public struct WaterBodyPart
{

    public Transform tran;

    public Mesh mesh;

    //public BuoyancyEffector2D effector;


    /// <summary>
    /// 
    /// </summary>
    public WaterBodyPart(Transform parent, Material mat, float xPos1, float xPos2, float yPos1, float yPos2, float zPos, float xUV1, float xUV2)
    {
        GameObject go = new GameObject("Water2D Part");   
        tran = go.transform;
        tran.SetParent(parent);
        go.transform.localScale = Vector3.one;
        go.transform.localPosition = Vector3.right * xPos1;
        go.layer = 4;

        mesh = new Mesh();
        mesh.MarkDynamic();
        go.AddComponent<MeshFilter>().mesh = mesh;

        MeshRenderer mr = go.AddComponent<MeshRenderer>();
        mr.material = mat;
        mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        mr.receiveShadows = false;
        mr.useLightProbes = false;
        mr.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;     

        UpdateVertices(xPos1, xPos2, yPos1, yPos2, zPos);
        mesh.triangles = new int[] { 0, 1, 3, 3, 2, 0 };
        mesh.uv = new Vector2[] { new Vector2(xUV1, 1), new Vector2(xUV2, 1), new Vector2(xUV1, 0), new Vector2(xUV2, 0) };
    }


    /// <summary>
    /// 
    /// </summary>
    public void UpdateVertices(float xPos1, float xPos2, float yPos1, float yPos2, float zPos)
    {
        Vector3[] vert = new Vector3[4];
        vert[0] = new Vector3(xPos1, yPos1, zPos) - tran.localPosition;
        vert[1] = new Vector3(xPos2, yPos2, zPos) - tran.localPosition;
        vert[2] = new Vector3(xPos1, 0, zPos) - tran.localPosition;
        vert[3] = new Vector3(xPos2, 0, zPos) - tran.localPosition;

        mesh.vertices = vert;

    }

}
