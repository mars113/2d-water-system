﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Water2DSystem))]
public class Water2DSystemEditor : Editor
{

    public void OnEnable()
    {
        Water2DSystem t = target as Water2DSystem;

        t.gameObject.layer = 4;
    }

    public void OnSceneGUI()
    {
        DrawRect();
    }


    void DrawRect()
    {

        Water2DSystem  t = target as Water2DSystem;

        if (Application.isPlaying || !t.initParam.segmentPreview)
            return;

        Vector3 tran = t.transform.position;
        float z = t.transform.position.z + t.initParam.offsetZ;
        
        Vector3 rectTopLeft     = tran + new Vector3( t.initParam.width * -0.5f / t.initParam.pixelPerUnit * t.transform.localScale.x,     t.initParam.height / t.initParam.pixelPerUnit * t.transform.localScale.y,      z );
        Vector3 rectTopRight    = tran + new Vector3( t.initParam.width * 0.5f  / t.initParam.pixelPerUnit * t.transform.localScale.x,     t.initParam.height / t.initParam.pixelPerUnit * t.transform.localScale.y,      z );
        Vector3 rectBottomLeft  = tran + new Vector3( t.initParam.width * -0.5f / t.initParam.pixelPerUnit * t.transform.localScale.x,     0,                                                  z );
        Vector3 rectBottomRight = tran + new Vector3( t.initParam.width * 0.5f  / t.initParam.pixelPerUnit * t.transform.localScale.x,     0,                                                  z );
        
        if(t.initParam.waveCount > 2)
        {

            Handles.color = Color.cyan;

            float deltaX = (t.initParam.width / t.initParam.waveCount) / t.initParam.pixelPerUnit;

            for(int i = 0; i < t.initParam.waveCount + 1; i++)
            {
                Handles.DrawLine(rectTopLeft + Vector3.right * deltaX * i * t.transform.localScale.x, rectBottomLeft + Vector3.right * deltaX * i * t.transform.localScale.x);
            }
        }

        Handles.color = Color.yellow;
        Handles.DrawLine(rectTopLeft, rectTopRight);
        Handles.DrawLine(rectTopRight, rectBottomRight);
        Handles.DrawLine(rectBottomRight, rectBottomLeft);
        Handles.DrawLine(rectBottomLeft, rectTopLeft);

    }

}
