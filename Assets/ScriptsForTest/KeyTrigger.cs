﻿using UnityEngine;

public class KeyTrigger : MonoBehaviour
{

    public GameObject box;

    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            box.SetActive(true);

            Destroy(gameObject);
        }

    }
    


}
